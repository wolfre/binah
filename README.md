# Binah
 
An extendable command line parser with building blocks.
 
# Usage
 
## Simple command line parsing
 
First you define a scheme with everything you like to parse ...
 
```csharp
using Binah;
 
enum SomeEnum
{
	Entry,
	OtherEntry
}

class CmdArgs
{
	[CommandLine("--some-text", "This is a text argument.")]
	public string txt = "Default value";
	
	[CommandLine("--enum-switch", "You can also use enum.")]
	public SomeEnum enm = SomeEnum.Entry;
	
	[CommandLine("--flag", "The field will be non-null if flag was given.")]
	public object someFlag = null;
	
	[CommandLine("--file", "A file maybe.")]
	public FileInfo someFile = new FileInfo("default.file");
	
	[CommandLine("--mandatory-switch", "This switch must be given, otherwise parsing throws an ArgumentException.", false)]
	public int someMandatoryNumber;
}
```

... then you can use it in main (or where ever you want) like so ...

```csharp
public static void Main(string[] args)
{
	var p = new CommandLineParser();
	var parsed = new CmdArgs();
	
	// Parse
	var unconsumendArgument = p.Args(parsed, args);
	
	// Now parsed is filled accordingly and may be used for what ever you want.
	// unconsumendArgument contains all the unmatched arguments from args
}
```

You can produce a help screen to the console for all arguments in `CmdArgs` like so

```csharp
public static void Main(string[] args)
{
	var parsed = new CmdArgs();
	var h = new PrintHelp(Console.WriteLine);
	h.ForScheme(parsed);
}
```

## Array arguments

For a scheme like so

```csharp
class CmdArgs
{
	[CommandLine("--number", "This switch can be used multiple times.")]
	public int[] numbers = new int[0];
}
```

... every occurence of the `--number` switch will extend the `numbers` array with one additional element.

## Action based app

If your app grows and you want to have multiple distinct things it can do, maybe an action based approach is what you are looking for.

```csharp
using Binah;

class CmdArgs
{
	// See above
}

// An action represents a certain *thing* your app can do
class Action1 : IAction<CmdArgs>
{
	public string Switch { get { return "action1"; } }

	public string Info { get { return "Some nice info about what this action does."; } }

	public void Run(CmdArgs args, string[] unconsumend)
	{
		// Do your thing here
	}
}

class OtherCmdArgs
{
	// Same as CmdArgs
}

// You can have as many actions as you need.
// They can have differnt argument schemes as well.
class Action2 : IAction<OtherCmdArgs>
{
	// See above
}
 
```

You can now use the `ActionApp` class to do all the parsing for you, like so.

```csharp
public static void Main(string[] args)
{
	var a1 = new Action1();
	var a2 = new Action2();

	var app = new ActionApp(Console.WriteLine)
	{
		AppName = "My App",
		AppInfo = "Some short info about what this app is.",
	};
	
	app.Add(a1);
	app.Add(a2);
	
	app.Run(args);
}
```

The first argument on the command line will chose the action.
Any further arguments will be parsed into the `T` from the `IAction<T>` interface that your actions implement.
Also this will now print meaningfull help messages if run without args, or with the usuall `-h` or `--help` switches.

## Custom parsers

You can extend the parser with new types.

```csharp
class MyType
{
	public static MyType Parse(string arg)
	{
		// parse your fancy type here
	}
}
```

Then you have to stack up parsing layers your self, like so `IValueParser` -> `ICommandLineParser` -> `ActionApp`:

```csharp
public static void Main(string[] args)
{
	var vParser = new Binah.Parsing.ValueParser();
	vParser.Extend(typeof(MyType), MyType.Parse);
	
	var cmdParser = new Binah.CommandLineParser(vParser); 
	var app = new ActionApp(cmdParser, Console.WriteLine);
	
	// add actions
	
	app.Run(args);
}
```

# Change log

## V2.0.4

- Support GitlabCI

## V2.0.3

- Fix .gitignore

## V2.0.2

- Proper version extraction from tags
- Dockerize things and get rid of $TOOLS injection

## V2.0.1

- Fix - Array defaults are not serialized properly

## V2.0.0

- First properly packaged version
