﻿// ***************************************************************
// Copyright (c) 2015-2019, Rene Wolf
// All rights reserved.
// Part of Binah, see License.txt for full license text
// ***************************************************************

using System;
using System.Linq;
using System.Reflection;

namespace Binah.Parsing
{
    public class ArgumentPackage
    {
        public CommandLineAttribute attr;
        public FieldInfo field;


        public static ArgumentPackage[] GetPackages(Type t)
        {
            var fieldsWithAttributes = t.GetFields()
                .Where(f => f.GetCustomAttributes().Any(a => a.GetType() == typeof(CommandLineAttribute)))
                .ToArray();

            return fieldsWithAttributes.Select(f => new ArgumentPackage()
            {
                field = f,
                attr = (CommandLineAttribute)(f.GetCustomAttributes()
                    .Single(
                        a => a.GetType() == typeof(CommandLineAttribute)))
            }).ToArray();
        }
    }
}
