using System.Reflection;

// NOTE this is a placeholder and will be generated by build.sh

[assembly: AssemblyVersion("0.0.0.0")]
[assembly: AssemblyFileVersion("0.0.0.0")]
[assembly: AssemblyInformationalVersion("0.0.0")]

static class BuildConsts
{
    public const string GitCommit = "0000000000000000000000000000000000000000";
    public const string GitCommitShort = "00000000";
    public const string BuildInfo = "info about this build";
}
