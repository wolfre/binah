﻿// ***************************************************************
// Copyright (c) 2015-2021, Rene Wolf
// All rights reserved.
// Part of Binah, see License.txt for full license text
// ***************************************************************

using System.Reflection;
using System.Runtime.CompilerServices;

// Information about this assembly is defined by the following attributes.
// Change them to the values specific to your project.

[assembly: AssemblyTitle("Binah")]
[assembly: AssemblyDescription("Command line parser")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Binah")]
[assembly: AssemblyCopyright("Rene Wolf 2015-2021")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

