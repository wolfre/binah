﻿// ***************************************************************
// Copyright (c) 2015-2019, Rene Wolf
// All rights reserved.
// Part of Binah, see License.txt for full license text
// ***************************************************************

using System;
using System.Collections.Generic;
using Binah;
using NUnit.Framework;

namespace Test
{
    [TestFixture]
    public class ActionAppTest
    {
        class CmdArgs
        {
            [CommandLine("-s", "Some stuff about this argument.")]
            public string txt = "";
            [CommandLine("--just-for-fillers", "Also some info about this argument.")]
            public bool filler = false;
        }

        class Action1 : IAction<CmdArgs>
        {
            public string Switch => "act1";

            public string Info => "Some info on the action";

            public CmdArgs a;
            public string[] unc;

            public void Run(CmdArgs args, string[] unconsumend)
            {
                a = args;
                unc = unconsumend;
            }
        }

        class Action2 : IAction<CmdArgs>
        {
            public string Switch => "act2";

            public string Info => "Some info on the action";

            public void Run(CmdArgs args, string[] unconsumend)
            {
                throw new AssertionException("Should not have been called");
            }
        }

        [Test]
        public void Help([Values("-h", "--help", "")] string swi)
        {
            var p = new List<string>();

            var a1 = new Action1();
            var a2 = new Action2();

            const string Name = "The App";
            const string Info = "Some info about your app";

            var app = new ActionApp(p.Add)
            {
                AppName = Name,
                AppInfo = Info,
            };
            app.Add(a1);
            app.Add(a2);

            if (swi == "")
            {
                app.Run();
            }
            else
            {
                app.Run(swi);
            }

            foreach (var l in p)
                Console.WriteLine(l);

            var allTxt = string.Join("\n", p);

            Assert.That(allTxt, Contains.Substring(a1.Info));
            Assert.That(allTxt, Contains.Substring(a1.Switch));
            Assert.That(allTxt, Contains.Substring(a2.Info));
            Assert.That(allTxt, Contains.Substring(a2.Switch));

            Assert.That(allTxt, Contains.Substring(Name));
            Assert.That(allTxt, Contains.Substring(Info));
        }


        [Test]
        public void Run()
        {
            var p = new List<string>();

            var a1 = new Action1();
            var a2 = new Action2();

            var app = new ActionApp(p.Add);
            app.Add(a1);
            app.Add(a2);

            app.Run(a1.Switch, "something", "-s", "hello");

            Assert.That(a1.a.txt, Is.EqualTo("hello"));
            Assert.That(a1.unc.Length, Is.EqualTo(1));
            Assert.That(a1.unc[0], Is.EqualTo("something"));
        }

        [Test]
        public void UnknownAction()
        {
            const string Action = "oops";

            var app = new ActionApp(Console.WriteLine);
            var exc = Assert.Throws<ArgumentException>(() => app.Run(Action));

            Assert.That(exc.Message, Contains.Substring(Action));
        }
    }
}
