﻿// ***************************************************************
// Copyright (c) 2015-2019, Rene Wolf
// All rights reserved.
// Part of Binah, see License.txt for full license text
// ***************************************************************

using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace Binah.Parsing
{
    public class ValueParser : IValueParser
    {
        protected static double ParseFloat(string n)
        {
            if (double.TryParse(n, out double i))
                return i;

            throw new FormatException("'" + n + "' does not look like a floating point number");
        }

        protected static Int64 ParseInt(string n)
        {
            if (Int64.TryParse(n, out long i))
                return i;

            throw new FormatException("'" + n + "' does not look like an signed integer number");
        }

        protected static UInt64 ParseUInt(string n)
        {
            if (UInt64.TryParse(n, out ulong i))
                return i;

            throw new FormatException("'" + n + "' does not look like an unsigned integer number");
        }

        protected static DirectoryInfo ParseDir(string dir)
        {
            return new DirectoryInfo(dir);
        }

        protected static FileInfo ParseFile(string dir)
        {
            return new FileInfo(dir);
        }

        protected static bool ParseBool(string arg)
        {
            string lowercase = arg.ToLowerInvariant();

            if (lowercase == "true")
                return true;

            if (lowercase == "false")
                return false;

            throw new FormatException("'" + arg + "' cannot be parsed to bool");
        }

        protected readonly Dictionary<Type, Func<string, object>> parser = new Dictionary<Type, Func<string, object>>()
        {
            { typeof(byte), (n) => (byte)ParseUInt(n) },
            { typeof(UInt16), (n) => (UInt16)ParseUInt(n) },
            { typeof(UInt32), (n) => (UInt32)ParseUInt(n) },
            { typeof(UInt64), (n) => (UInt64)ParseUInt(n) },

            { typeof(sbyte), (n) => (sbyte)ParseInt(n) },
            { typeof(Int16), (n) => (Int16)ParseInt(n) },
            { typeof(Int32), (n) => (Int32)ParseInt(n) },
            { typeof(Int64), (n) => (Int64)ParseInt(n) },

            { typeof(double), (n) => (double)ParseFloat(n) },
            { typeof(float), (n) => (float)ParseFloat(n) },

            { typeof(bool), b => ParseBool(b) },
            { typeof(Uri), u => new Uri(u) },
            { typeof(string), s => s },
            { typeof(DirectoryInfo), ParseDir },
            { typeof(FileInfo), ParseFile },
        };

        public virtual void Extend(Type valueType, Func<string, object> valueParser)
        {
            if (valueType == null)
                throw new ArgumentNullException(nameof(valueType));

            if (valueParser == null)
                throw new ArgumentNullException(nameof(valueParser));

            if (parser.ContainsKey(valueType))
                parser.Remove(valueType);

            parser.Add(valueType, valueParser);
        }

        public virtual object Single(Type valueType, string value)
        {
            // https://msdn.microsoft.com/en-us/library/ms366789.aspx
            if (valueType.IsGenericType && valueType.GetGenericTypeDefinition() == typeof(Nullable<>))
                return Single(valueType.GetGenericArguments().ElementAt(0), value);

            if (valueType.IsArray)
                return Single(valueType.GetElementType(), value);

            if (valueType.IsEnum)
            {
                var names = Enum.GetNames(valueType);
                if (names.Contains(value) == false)
                    throw new ArgumentOutOfRangeException(nameof(value), "'" + value + "'", "Unknown value (" + string.Join(", ", names) + ")");

                return Enum.Parse(valueType, value);
            }


            if (parser.ContainsKey(valueType) == false)
                throw new NotSupportedException("Parsing '" + value + "' of type " + valueType + " not supported, no parser for this type");

            return parser[valueType](value);
        }
    }
}

