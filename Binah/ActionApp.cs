﻿// ***************************************************************
// Copyright (c) 2015-2019, Rene Wolf
// All rights reserved.
// Part of Binah, see License.txt for full license text
// ***************************************************************

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Binah
{
    public class ActionApp
    {
        readonly string[] helpSwitches;
        readonly Action<string> writeLine;
        readonly ICommandLineParser parser;

        public ActionApp(Action<string> writeLine, params string[] helpSwitches)
            : this(new CommandLineParser(), writeLine, helpSwitches)
        {
        }

        public ActionApp(ICommandLineParser parser, Action<string> writeLine, params string[] helpSwitches)
        {
            if (parser == null)
                throw new ArgumentNullException(nameof(parser));
            if (writeLine == null)
                throw new ArgumentNullException(nameof(writeLine));

            if (helpSwitches == null)
                throw new ArgumentNullException(nameof(helpSwitches));

            if (helpSwitches.Length == 0)
                helpSwitches = new string[] { "-h", "--help" };

            this.helpSwitches = helpSwitches;
            this.writeLine = writeLine;
            this.parser = parser;
        }

        readonly Dictionary<IActionInfo, Type> act = new Dictionary<IActionInfo, Type>();

        public void Add<T>(IAction<T> action)
            where T : new()
        {
            if (act.ContainsKey(action) == false)
                act.Add(action, typeof(T));
        }

        /// <summary>
        /// The name of your app.
        /// </summary>
        public string AppName = "My App";

        /// <summary>
        /// A short text describing your app, maybe about one or two lines of text.
        /// </summary>
        public string AppInfo = "A nice small app that does things.";

        Assembly AppAssembly
        {
            get
            {
                // Thats guessing a bit ... we assume that the Actions come from the assembly of the executable.
                return act.Keys.First().GetType().Assembly;
            }
        }

        string ExeName
        {
            get
            {
                var uri = new Uri(AppAssembly.CodeBase);
                if (uri.IsFile == false)
                    return uri.ToString();

                return (new FileInfo(uri.LocalPath)).Name;
            }
        }

        void Help()
        {
            Action<string> l = writeLine;

            l(AppName);
            l("");
            l(AppInfo);
            l("");
            l("Usage:");
            l(ExeName + " <Action> [options]");
            l("");

            var p = new PrintHelp(ll => writeLine("  " + ll));

            foreach (var a in act)
            {
                l("Action '" + a.Key.Switch + "': " + a.Key.Info);
                p.ForScheme(Activator.CreateInstance(a.Value));
                l("");
            }
        }

        public void Run(params string[] args)
        {
            if (args == null || args.Length == 0 || args.Any(a => helpSwitches.Any(h => h == a)))
            {
                Help();
                return;
            }

            var fittingAction = act.Where(a => a.Key.Switch == args[0]).ToArray();
            if (fittingAction.Length == 0)
                throw new ArgumentException("Unknown action '" + args[0] + "'");

            var action = fittingAction.First();
            var scheme = Activator.CreateInstance(action.Value);
            var unconsumed = parser.Args(scheme, args.Skip(1).ToArray());

            var runFunc = action.Key.GetType().GetMethod(nameof(IAction<object>.Run));
            runFunc.Invoke(action.Key, new object[] { scheme, unconsumed });
        }
    }
}
