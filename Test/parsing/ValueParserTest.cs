﻿// ***************************************************************
// Copyright (c) 2015-2019, Rene Wolf
// All rights reserved.
// Part of Binah, see License.txt for full license text
// ***************************************************************

using NUnit.Framework;
using System;
using Binah.Parsing;
using System.IO;

#pragma warning disable 0414

namespace Test.parsing
{
    [TestFixture]
    public class ValueParserTest
    {
        object P(Type t, string x)
        {
            var p = new ValueParser();
            return p.Single(t, x);
        }

        readonly static TestCaseData[] goodcaseTests =
        {
            new TestCaseData( typeof(UInt32), "42", (UInt32)42 ),
            new TestCaseData( typeof(Int16), "-5", (Int16)(-5) ),
            new TestCaseData( typeof(Uri), "http://host", new Uri("http://host")),
            new TestCaseData( typeof(string), "some string", "some string"),
            new TestCaseData( typeof(UInt32?), "42", (UInt32)42 ),
            new TestCaseData( typeof(UInt64), "42", (UInt64)42 ),
            new TestCaseData( typeof(bool), "true", true ),
            new TestCaseData( typeof(bool), "True", true ),
            new TestCaseData( typeof(bool), "false", false ),
            new TestCaseData( typeof(bool), "False", false ),
            new TestCaseData( typeof(string[]), "bla", "bla" ),
            new TestCaseData( typeof(double), "0.5", (double)0.5 ),
            new TestCaseData( typeof(double), "-0.5", (double)-0.5 ),
            new TestCaseData( typeof(double), "+0.5", (double)0.5 ),
            new TestCaseData( typeof(float), "0.5", (float)0.5 ),
            new TestCaseData( typeof(float), "-0.5", (float)-0.5 ),
            new TestCaseData( typeof(float), "+0.5", (float)0.5 ),
        };

        [Test]
        [TestCaseSource(nameof(goodcaseTests))]
        public void SingleGoodCase(Type t, string ok, object expected)
        {
            Assert.That(P(t, ok), Is.EqualTo(expected), t + " '" + ok + "'");
        }

        readonly static TestCaseData[] badcaseTests =
        {
            new TestCaseData( typeof(UInt32), "4.5" ),
            new TestCaseData( typeof(UInt32), "-5" ),
            new TestCaseData( typeof(double), "zonk" ),
            new TestCaseData( typeof(Uri), "bla blub" ),
            new TestCaseData( typeof(bool), "666" ),
        };

        [Test]
        [TestCaseSource(nameof(badcaseTests))]
        public void SingleBadCase(Type t, string illegal)
        {
            try
            {
                P(t, illegal);
            }
            catch (Exception)
            {
                return;
            }

            Assert.Fail(t + " '" + illegal + "' didn't throw");
        }

        class SomeCustomType
        {
            public string arg = "Default";
        }

        [Test]
        public void Extend()
        {
            var p = new ValueParser();
            p.Extend(typeof(SomeCustomType), s => new SomeCustomType() { arg = s });

            object parsed = p.Single(typeof(SomeCustomType), "Test");
            Assert.That(parsed, Is.InstanceOf(typeof(SomeCustomType)));
            Assert.That(((SomeCustomType)parsed).arg, Is.EqualTo("Test"));
        }

        readonly static TestCaseData[] dirTests =
        {
            new TestCaseData("/I/do/not/exist", false),
            new TestCaseData("/etc", true),
            new TestCaseData("/etc/", true),
        };

        [Test]
        [TestCaseSource(nameof(dirTests))]
        public void ParseDirectoryInfo(string path, bool exists)
        {
            var d = (DirectoryInfo)P(typeof(DirectoryInfo), path);
            Assert.That(d.Exists, Is.EqualTo(exists), "Exists");
        }



        [Test]
        public void ParseFileInfo()
        {
            var p = "/hello/world";
            var d = (FileInfo)P(typeof(FileInfo), p);
            Assert.That(d.FullName, Is.EqualTo(p));
        }

    }
}

