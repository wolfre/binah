﻿// ***************************************************************
// Copyright (c) 2015-2019, Rene Wolf
// All rights reserved.
// Part of Binah, see License.txt for full license text
// ***************************************************************

using NUnit.Framework;
using System;
using Binah;
using System.Collections.Generic;
using System.Linq;

#pragma warning disable CS0649

namespace Test
{

    [TestFixture]
    public class ParseTest
    {

        T Args<T>(params string[] args)
            where T : new()
        {
            var p = new CommandLineParser();
            var ret = new T();
            p.Args(ret, args);
            return ret;
        }


        class TestSwitch
        {
            [CommandLine("--help","this is a switch")]
            public object help;
        }

        [Test]
        public void ArgSwitchPresent()
        {
            TestSwitch tst = Args<TestSwitch>("--help");
            Assert.NotNull(tst.help, "set");
        }

        [Test]
        public void ArgSwitchAbsent()
        {
            var tst = Args<TestSwitch>("--other");
            Assert.Null(tst.help, "absent");
        }

        class TestBool
        {
            [CommandLine("--normal", "")]
            public bool normal = true;

            [CommandLine("--nullable", "")]
            public bool? nullable;
        }

        [Test]
        public void ValueTypeDefault()
        {
            var tst = Args<TestBool>("--other");
            Assert.That(tst.normal, Is.EqualTo(new TestBool().normal), "default");
        }

        [Test]
        public void ValueTypeNullableDefault()
        {
            var tst = Args<TestBool>("--other");
            Assert.That(tst.nullable, Is.EqualTo(new TestBool().nullable), "default");
        }

        [Test]
        public void ValueType()
        {
            var tst = Args<TestBool>("--normal", "false");
            Assert.That(tst.normal, Is.EqualTo(false));
        }

        [Test]
        public void ValueTypeNullable()
        {
            var tst = Args<TestBool>("--nullable", "true");
            Assert.That(tst.nullable, Is.EqualTo(true), "normal");
        }

        class Consumer
        {
            [CommandLine("-a", "")]
            public UInt32 a;

            [CommandLine("-b", "")]
            public object flag;
        }

        [Test]
        public void UnConsumed()
        {
            var p = new CommandLineParser();

            var tst = new Consumer();
            var unconsumed = p.Args(tst, "Not1", "-b", "Not2", "-a", "42", "Not3");

            Assert.That(tst.a, Is.EqualTo(42));
            Assert.NotNull(tst.flag, "flag");

            Assert.That(unconsumed.Count, Is.EqualTo(3), "Count");
            Assert.That(unconsumed[0], Is.EqualTo("Not1"));
            Assert.That(unconsumed[1], Is.EqualTo("Not2"));
            Assert.That(unconsumed[2], Is.EqualTo("Not3"));
        }

        class ArrayTest
        {
            [CommandLine("-s", "")]
            public string[] arrayStr = null;

            [CommandLine("-n", "")]
            public Int32[] arrayNum = null;
        }

        [Test]
        public void ArrayTypes()
        {
            var tst = Args<ArrayTest>("-n", "1", "-s", "bla", "-n", "2");

            Assert.That(tst.arrayStr.Length, Is.EqualTo(1), "len str");
            Assert.That(tst.arrayStr[0], Is.EqualTo("bla"));

            Assert.That(tst.arrayNum.Length, Is.EqualTo(2), "len num");
            Assert.That(tst.arrayNum[0], Is.EqualTo(1), "num[0] " + Nice(tst.arrayNum));
            Assert.That(tst.arrayNum[1], Is.EqualTo(2), "num[1] " + Nice(tst.arrayNum));
        }

        static string Nice(Int32[] data)
        {
            if (data == null)
                return "null";
            return "[" + string.Join(", ", data.Select(e => "" + e)) + "]";
        }


        class SomethingMandatory
        {
            [CommandLine("--int", "stuff", false)]
            public int someInt = 42;
        }

        [Test]
        public void MandatoryMissing()
        {
            var exc = Assert.Throws<ArgumentException>(() => Args<SomethingMandatory>("--some-other-switch", "666"));
            Assert.That(exc.Message, Contains.Substring("--int").And.ContainsSubstring("stuff"));
        }
    }
}

#pragma warning restore CS0649
