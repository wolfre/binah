#!/bin/bash

source common.sh.source

get_environment
print_workspace_state

NUNIT_CONSOLE="$(pwd)/$(find . -iname "*nunit*console*.exe" | head -n 1)"
if [[ ! -f "$NUNIT_CONSOLE" ]] ; then die "Couldn't find nunit console: '$NUNIT_CONSOLE'?!" ; fi

cd "$BUILD_DIR"

xslt_url="https://raw.githubusercontent.com/nunit/nunit-transforms/master/nunit3-junit/nunit3-junit.xslt"
xslt=nunit3-junit.xslt
echo "About to get the XSLT to transform NUnit3 -> JUnit results ..."
curl "$xslt_url" > $xslt
md5sum $xslt
cat $xslt | grep -q "xsl:stylesheet version=" || die "'$xslt' does not seem useable"

mono "$NUNIT_CONSOLE" "Test.dll" --labels=Before "--result=nunit-results.nunit3.xml;format=nunit3" "--result=nunit-results.junit.xml;transform=$xslt"

# we delete the xslt as we don't want to accidentally include it in the artefacts (legal precaution)
rm $xslt

exit 0
