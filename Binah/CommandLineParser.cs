﻿// ***************************************************************
// Copyright (c) 2015-2019, Rene Wolf
// All rights reserved.
// Part of Binah, see License.txt for full license text
// ***************************************************************

using System;
using System.Reflection;
using System.Linq;
using System.Collections.Generic;
using Binah.Parsing;

namespace Binah
{
    public class CommandLineParser : ICommandLineParser
    {
        public CommandLineParser()
            : this(new ValueParser())
        {
        }

        readonly IValueParser singleValueParser;

        public CommandLineParser(IValueParser singleValueParser)
        {
            this.singleValueParser = singleValueParser;
        }

        public string[] Args(object fillThisScheme, params string[] args)
        {
            var switches = ArgumentPackage.GetPackages(fillThisScheme.GetType())
                .ToDictionary((o) => o.attr.CmdSwitch);

            var cmdQ = new List<string>(args);
            var usedSwitches = new List<ArgumentPackage>();
            var unconsumed = new List<string>();

            while (cmdQ.Count > 0)
            {
                string arg = cmdQ.ElementAt(0);
                cmdQ.RemoveAt(0);

                if (switches.ContainsKey(arg) == false)
                {
                    // something else
                    unconsumed.Add(arg);
                }
                else
                {
                    var pkg = switches[arg];
                    if (usedSwitches.Contains(pkg) == false)
                        usedSwitches.Add(pkg);

                    object val;

                    if (pkg.field.FieldType == typeof(object))
                    {
                        val = new object();
                    }
                    else
                    {
                        if (cmdQ.Count < 1)
                            throw new ArgumentOutOfRangeException("Option " + pkg.attr.CmdSwitch + " needs an agrument of type " + pkg.field.FieldType);

                        val = singleValueParser.Single(pkg.field.FieldType, cmdQ.ElementAt(0));
                        cmdQ.RemoveAt(0);
                    }

                    if (pkg.field.FieldType.IsArray)
                    {
                        AddToArray(fillThisScheme, pkg.field, val);
                    }
                    else
                    {
                        pkg.field.SetValue(fillThisScheme, val);
                    }
                }
            }

            var missingMandatorySwitches = switches
                .Select(p => p.Value)
                .Where(p => p.attr.Optional == false)
                .Where(p => usedSwitches.Contains(p) == false)
                .ToArray();

            if (missingMandatorySwitches.Length == 0)
                return unconsumed.ToArray();

            throw new ArgumentException("Missing options: " + string.Join("\n", missingMandatorySwitches.Select(s => s.attr.CmdSwitch + " - " + s.attr.Description)));
        }


        static void AddToArray(object obj, FieldInfo field, object newVal)
        {
            object arr = field.GetValue(obj);
            Type element = field.FieldType.GetElementType();

            if (arr == null)
                arr = Array.CreateInstance(element, 0);

            int len = ((Array)arr).GetLength(0);
            object extArr = Array.CreateInstance(element, len + 1);

            Array.Copy((Array)arr, (Array)extArr, len);
            ((Array)extArr).SetValue(newVal, len);

            field.SetValue(obj, extArr);
        }

    }

}

