﻿// ***************************************************************
// Copyright (c) 2015-2019, Rene Wolf
// All rights reserved.
// Part of Binah, see License.txt for full license text
// ***************************************************************

using System;
using System.Linq;
using Binah.Parsing;

namespace Binah
{

    public class PrintHelp
    {
        readonly Action<string> writeLine;

        public PrintHelp(Action<string> writeLine)
        {
            this.writeLine = writeLine;
        }

        // TODO auto calc this
        readonly int Width = 40;
        public string Indent = "  ";

        public void ForScheme(object defaultValues)
        {
            foreach (var argPkg in ArgumentPackage.GetPackages(defaultValues.GetType()))
                OneSwitch(defaultValues, argPkg);
        }

        void OneSwitch(object defaultValues, ArgumentPackage argPkg)
        {
            var attribute = argPkg.attr;
            var fType = argPkg.field.FieldType;
            var isSimpleSwitch = fType == typeof(object);

            var sw = attribute.CmdSwitch;
            if (isSimpleSwitch == false)
                sw += " <" + TypeToString(fType) + ">";

            writeLine(PadRight(sw, Width) + attribute.Description);

            // A simple switch only has a desctiption, but a default value or being mandatory makes no sense.
            if (isSimpleSwitch)
                return;


            var indentText = PadRight("", Width);
            var fieldType = ResolveNullable(fType);
            if (fieldType.IsEnum)
                writeLine(indentText + Indent + "Enum { " + string.Join(", ", Enum.GetNames(fieldType)) + " }");

            if (argPkg.attr.Optional == false)
            {
                writeLine(indentText + Indent + "This switch is mandatory");
            }
            else
            {
                var dValue = argPkg.field.GetValue(defaultValues);
                writeLine(indentText + Indent + "Default: " + DefaultValueToString(dValue));
            }
        }

        static string DefaultValueToString(object def)
        {
            if (def == null)
                return "not set";

            var t = def.GetType();
            if (t.IsArray)
            {
                var arr = (Array)def;

                return "[" + string.Join(",", Enumerable
                    .Range(0, arr.Length)
                    .Select(i => arr.GetValue(i))
                    .Select(DefaultValueToString)) + "]";
            }

            if (t == typeof(string))
            {
                var str = (string)def;
                var q = str.Contains('"') ? "'" : "\"";
                return q + str + q;
            }

            return def.ToString();
        }

        static string PadRight(string s, int len)
        {
            if (s.Length >= len)
                return s;

            return s.PadRight(len);
        }

        static string TypeToString(Type t)
        {
            return ResolveNullable(t).Name;
        }

        static Type ResolveNullable(Type t)
        {
            // https://msdn.microsoft.com/en-us/library/ms366789.aspx
            if (t.IsGenericType && t.GetGenericTypeDefinition() == typeof(Nullable<>))
                return ResolveNullable(t.GetGenericArguments()[0]);

            return t;
        }
    }
}
