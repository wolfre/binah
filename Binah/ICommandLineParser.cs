﻿// ***************************************************************
// Copyright (c) 2015-2019, Rene Wolf
// All rights reserved.
// Part of Binah, see License.txt for full license text
// ***************************************************************

using System;

namespace Binah
{
    /// <summary>
    /// Parses command line options into a scheme, based on attributs.
    /// </summary>
    public interface ICommandLineParser
    {
        /// <summary>
        /// Parses arguments based on Commandline attributes of a given object.
        /// </summary>
        /// <returns>Any unconsumed aguments</returns>
        /// <param name="fillThisScheme">Fill parsed command line arguments into this object.</param>
        /// <param name="args">Arguments from the commandline.</param>
        string[] Args(object fillThisScheme, params string[] args);
    }
}
