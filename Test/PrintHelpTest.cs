﻿// ***************************************************************
// Copyright (c) 2015-2019, Rene Wolf
// All rights reserved.
// Part of Binah, see License.txt for full license text
// ***************************************************************

using System;
using System.Collections.Generic;
using Binah;
using NUnit.Framework;


namespace Test
{
    [TestFixture]
    public class PrintHelpTest
    {
        public enum SomeEnum
        {
            Entry,
            OtherEntry
        }

#pragma warning disable CS0649

        class TestPrintHelp
        {
            [CommandLine("--int", "Some Int with default")]
            public int someInt = 42;

            [CommandLine("--help", "This is a switch")]
            public object help;

            [CommandLine("--nullableBool", "Does stuff if selected")]
            public bool? nullableBool;

            [CommandLine("--normalBool", "Does stuff if set to false")]
            public bool normalBool = true;

            [CommandLine("--enum", "This is an enum")]
            public SomeEnum enm = SomeEnum.OtherEntry;

            [CommandLine("--nullable-enum", "This is also an enum")]
            public SomeEnum? nullEnm;

            [CommandLine("-m", "Something importand", false)]
            public int mandatoryInt = 666;
        }

#pragma warning restore CS0649

        [Test]
        public void PrintHelp()
        {
            var p = new List<string>();
            var h = new PrintHelp(p.Add);

            h.ForScheme(new TestPrintHelp());

            foreach (var l in p)
                Console.WriteLine(l);

            var all = string.Join("\n", p);

            var allAtt = Binah.Parsing.ArgumentPackage.GetPackages(typeof(TestPrintHelp));
            foreach (var pkg in allAtt)
                Assert.That(all, Contains.Substring(pkg.attr.CmdSwitch).And.ContainsSubstring(pkg.attr.Description));

            Assert.False(all.Contains("666"), "The default value of a mandatory switch was printed");
            Assert.That(all, Contains.Substring("mandatory"));
        }


        class ArrayTest
        {
            [CommandLine("-s", "")]
            public string[] arrayStr = new string[] { "str1", "str\"2" };

            [CommandLine("-n", "")]
            public Int32[] arrayNum = new int[] { 42, 23, 666 };
        }

        [Test]
        public void ArrayDefaults()
        {
            var hh = "";
            var h = new PrintHelp(l => hh += "\n" + l );
            h.ForScheme(new ArrayTest());
            Console.WriteLine(hh);

            Assert.That(hh, Contains.Substring("[42,23,666]"));
            Assert.That(hh, Contains.Substring("[\"str1\",'str\"2']"));
        }
    }
}
