﻿// ***************************************************************
// Copyright (c) 2015-2019, Rene Wolf
// All rights reserved.
// Part of Binah, see License.txt for full license text
// ***************************************************************

using System;

namespace Binah
{
    public class CommandLineAttribute : Attribute
    {
        public CommandLineAttribute(string cmdSwitch, string description, bool optional = true)
        {
            Description = description;
            CmdSwitch = cmdSwitch;
            Optional = optional;
        }

        public string Description { private set; get; }
        public string CmdSwitch { private set; get; }
        public bool Optional { private set; get; }
    }
}

