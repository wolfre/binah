#!/bin/bash

source common.sh.source

get_environment
print_workspace_state

nuget pack Binah.nuspec -Version $VERSION_SEMANTIC -OutputDirectory "$BUILD_DIR/"

exit 0
