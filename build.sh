#!/bin/bash

source common.sh.source

get_environment

function gen_build_info_cs
{
	local cs="$1"
	
	if [[ ! -f "$cs" ]] ; then
		die "Placeholder '$cs' not found"
	fi
	
	echo "using System.Reflection;" > "$cs"
	echo "[assembly: AssemblyVersion(\"$VERSION_MS\")]" >> "$cs"
	echo "[assembly: AssemblyFileVersion(\"$VERSION_MS\")]" >> "$cs"
	echo "[assembly: AssemblyInformationalVersion(\"$VERSION_SEMANTIC\")]" >> "$cs"
	echo "static class BuildConsts {" >> "$cs"
	echo "public const string GitCommit = \"$GIT_COMMIT\";" >> "$cs"
	echo "public const string GitCommitShort = \"$GIT_COMMIT_SHORT\";" >> "$cs"
	echo "public const string BuildInfo = \"$INFO_BUILD\";"  >> "$cs"
	echo "}" >> "$cs"
	
	echo "<buildinfo file=\"$cs\">"
	cat "$cs"
	echo "</buildinfo>"
}

function try_revert_build_info_cs
{
	# this may fail on CI runs, as our docker container probably does not have git installed
	git checkout -- "$1"
}

gen_build_info_cs BuildInfo.cs

echo Will build solution now

nuget restore Test/packages.config -outputdirectory packages
msbuild /p:Configuration=Release /p:Platform="Any CPU" "$SOLUTION"
_exit=$?

# Reverting the  BuildInfo.cs  is to avoid changes to tracked files.
#  This helps with local development and also the (generated) content in this is a temporary thing ...
try_revert_build_info_cs BuildInfo.cs
print_workspace_state

test "$_exit" == "0" || die "MSBuild exit $_exit"
test -f "$BUILD_DIR/Binah.dll" || die "Build didn't produce output"
