﻿// ***************************************************************
// Copyright (c) 2015-2019, Rene Wolf
// All rights reserved.
// Part of Binah, see License.txt for full license text
// ***************************************************************

using System;

namespace Binah
{
    public interface IActionInfo
    {
        string Switch { get; }
        string Info { get; }
    }

    public interface IAction<T> : IActionInfo
        where T : new()
    {
        void Run(T args, string[] unconsumend);
    }
}
