﻿using System;
namespace Binah.Parsing
{
    /// <summary>
    /// Parses single values from strings into types.
    /// </summary>
    public interface IValueParser
    {
        /// <summary>
        /// Prases a single value into a type.
        /// </summary>
        /// <returns>The value parsed to the desired type.</returns>
        /// <param name="valueType">The desired type.</param>
        /// <param name="value">A value from a command line.</param>
        object Single(Type valueType, string value);

        /// <summary>
        /// Extend this parsed with a new type, or override the behavior for an existing type.
        /// </summary>
        /// <param name="valueType">Target type.</param>
        /// <param name="valueParser">A value parser for this type.</param>
        void Extend(Type valueType, Func<string, object> valueParser);
    }
}
